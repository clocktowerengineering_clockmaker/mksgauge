import time
from datetime import datetime
import serial
import traceback

class MKSGauge:

    defaultNodeID = '@253'
    terminator = ';FF'
    defaultBaud = 57600
    defaultTimeout = .1
    defaultPortID = '/dev/tty_ChamberGauge'
    
    # Defines here
    pressure = 'PR3'
    setpoint1Status = 'SS1'
    setpoint1Hysteresis = 'SH1'
    setpoint1Enable = 'EN1'
    setpoint1Direction = 'SPD1'
    setpoint2Status = 'SS2'
    setpoint2Hysteresis = 'SH2'
    setpoint2Enable = 'EN2'
    setpoint2Direction = 'SPD2'
    setpoint3Status = 'SS3'
    setpoint3Hysteresis = 'SH3'
    setpoint3Enable = 'EN3'
    setpoint3Direction = 'SPD3'
    
    
    # Init object
    def __init__(self):
        print('init!')

    # Open serial object #TODO: speed up port
    def StartSerial(self,serialPort=defaultPortID,baud=defaultBaud,timeout=defaultTimeout):
        self.gaugeSerial = serial.Serial()
        self.gaugeSerial.timeout=timeout
        self.gaugeSerial.baudrate=baud
        self.gaugeSerial.port=serialPort
        self.gaugeSerial.open()
                
        #print self.gaugeSerial.name
        return self.gaugeSerial
    
    # Close serial object 
    def StopSerial(self):
        self.gaugeSerial.close()

    
    # set command
    def Set(self,command,value=""):
        commandstring = self.defaultNodeID+command+'!'+value+self.terminator
        print(str(datetime.now())+ ' ' + commandstring)
        self.gaugeSerial.write(commandstring.encode())
        response = self.gaugeSerial.read_until(self.terminator).decode()
        print(str(datetime.now()) + ' '  + commandstring + ' ' + response)
        time.sleep(.01)
        return response            
        
        
    # get command
    def Get(self,command):
        commandstring=self.defaultNodeID+command+'?'+self.terminator
        
        print(str(datetime.now())+ ' ' + commandstring)
        self.gaugeSerial.write(commandstring.encode())
        response = (self.gaugeSerial.read_until(self.terminator)).decode()
        print(str(datetime.now()) + ' '  + commandstring + ' ' + response)
        time.sleep(.01)
        return response
 
    #get pressure
    def GetPressure(self,pres = pressure):
        #print(str(datetime.now()))
        pressurestring = self.Get(pres)
        print(str(datetime.now())+' ' + pressurestring)
        try:
            if len(pressurestring) < 5:
                response = -1214
            else:
                response = float(pressurestring[7:14])
        except:
            traceback.print_exc()
            self.gaugeSerial.readall();
            response = -666

        print(response)

        return response

        
    
